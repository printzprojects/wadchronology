import json
import urllib

# Top level directories.
TOP_DIRS = [
    'levels/',
    'themes/'
]

# Base of all API URLs
URL_BASE = 'https://www.doomworld.com/idgames/api/api.php?'


def json_from_url(url):
    """
    Load JSON from an URL
    :param url:
    :return:
    """
    response = urllib.urlopen(url)
    return json.loads(response.read())


def make_url(command, **kwargs):
    """
    Builds an API URL from kwargs
    :param command:
    :param kwargs:
    :return:
    """
    return URL_BASE + urllib.urlencode(dict({'action': command, 'out': 'json'}, **kwargs))


def make_array(obj):
    """
    Returns an array if obj is not an array
    :param obj:
    :return:
    """
    return [obj] if not isinstance(obj, list) else obj


def get_subdirs(directory):
    """
    Gets all subdirs, recursively, from dir.
    :param directory:
    :return:
    """
    print 'Getting subdirectories from', directory
    url = make_url('getdirs', name=directory)
    json_data = json_from_url(url)
    subdirs = []
    try:
        dir_list = make_array(json_data['content']['dir'])
        for subdirectory in dir_list:
            if 'name' in subdirectory:
                name = subdirectory['name']
                subdirs.append(name)
                subdirs += get_subdirs(name)    # recursive travel
    except KeyError:
        pass
    return subdirs


def get_files(directory):
    print 'Getting files from', directory
    url = make_url('getfiles', name=directory)
    json_data = json_from_url(url)
    files = []
    try:
        file_list = make_array(json_data['content']['file'])
        for file_entry in file_list:
            files.append({key: file_entry[key] for key in ('age', 'url')})
    except KeyError:
        pass
    return files


files = []
for top_dir in TOP_DIRS:
    dirs = get_subdirs(top_dir)
    for directory in dirs:
        if 'deathmatch' in directory:
            continue
        files += get_files(directory)
files.sort(key=lambda x: x['age'])
for file in files:
    print file['url']